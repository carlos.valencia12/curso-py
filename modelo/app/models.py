from mailbox import NoSuchMailboxError
from django.db import models

# Create your models here.
class Nivel(models.Model):
        maternal = models.CharField(max_length=50)
        preescolar = models.CharField(max_length=50)
        primaria = models.CharField(max_length=50)
        secundaria = models.CharField(max_length=50)
        

def __str__(self):
        return 

def __unicode__(self):
        return 

class Carrera (models.Model):
    nombre = models.CharField(max_length=50)
    rvoe = models.CharField(max_length=50)
    modalidad = models.CharField(max_length=50)
    semestre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

class Alumno(models.Model):
    nombre= models.CharField(max_length=50) 
    apellido_paterno= models.CharField(max_length=50) 
    apellido_materno= models.CharField(max_length=50) 
    n_control= models.CharField(max_length=50) 
    edad= models.IntegerField()    
    sexo= models.CharField(max_length=6)
    direccion = models.TextField()
    carrera = models.ForeignKey(Carrera, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.nombre
