from django.contrib import admin
from app.models import Alumno, Carrera

# Register your models here.
@admin.register(Alumno)
class AlumnoAdmin(admin.ModelAdmin):
    fields = [
        "nombre",
        "apellido_paterno",
        "apellido_materno",
        "n_control",
        "edad",
        "sexo",
        "direccion",
        "carrera",
    ]
@admin.register(Carrera)
class CarreraAdmin(admin.ModelAdmin):
    fields = [
        "nombre",
        "rvoe",
        "modalidad",
        "semestre",
    ]
