class Animal:
    nombre = ""
    raza = ""
    color = ""
    num_ojos = 0
    num_patas = 0

    def __init__(
        self,
        nombre,
        raza,
        color,
        num_ojos,
        num_patas
    ):
        self.nombre = nombre
        self.raza = raza
        self.color = color
        self.num_ojos = num_ojos
        self.num_patas = num_patas

    def comer(self):
        print("Gracias")


class Gato(Animal):
    
    def mauyar(self):
        print("Miau")
    
    def __str__(self):
        return self.color

class Perro(Animal):

    def ladrar(self):
        print("Huau")
    
    def __str__(self):
        return self.color

labrador = Perro(
    nombre="Pancho",
    raza="Labrador",
    color = "cafe",
    num_ojos = 4,
    num_patas = 4
)
dalmata = Perro(
    nombre="Dam",
    raza="Dalmata",
    color = "blanco con negro",
    num_ojos = 2,
    num_patas = 4
)

gato_uno = Gato(
    nombre="Pepe",
    raza="gato_uno",
    color = "blanco con negro",
    num_ojos = 2,
    num_patas = 4
)

print(labrador.__dict__)
labrador.ladrar()
print(dalmata.__dict__)
print(gato_uno.__dict__)
gato_uno.comer()
